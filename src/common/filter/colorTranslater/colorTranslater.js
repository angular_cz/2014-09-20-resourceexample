(function(angular) {
  'use strict';

  /* Filters */

  angular.module('cz.teckovany.filter.colorTranslater', []).
    filter('colorTranslation', function() {

      var colorTranslations = {
        'white': 'Bílá',
        'bright-green': 'Zářivě zelená',
        'vanila': 'Vanilková',
        'pale-blue': 'Světle modrá',
        'pale-yellow': 'Světle žlutá',
        'light-blue': 'Pastelově modrá',
        'bright-yellow': 'Zářivě žlutá',
        'blue': 'Modrá',
        'dark-orange': 'Tmavě oranžová',
        'bright-blue': 'Zářivě modrá',
        'light-pink': 'Světle růžová',
        'dark-blue': 'Tmavě modrá',
        'pink': 'Růžová',
        'dark-purple': 'Tmavě fialová',
        'bring-pink': 'Zářivě růžová',
        'brown': 'Hnědá',
        'red': 'Červená',
        'dark-brown': 'Tmavě hnědá',
        'dark-red': 'Tmavě červená',
        'grey': 'Šedá',
        'lime-green': 'Lipově zelená',
        'black': 'Černá'
      };

      return function(color) {
        return colorTranslations[color];
      };
    });
})(window.angular);