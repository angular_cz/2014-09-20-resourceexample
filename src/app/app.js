(function(angular) {
  'use strict';

// Declare app level module which depends on filters, and services
  var app = angular.module('administration', [
    'ngRoute',
    'ngResource',
    'administration.orders',
    'cz.teckovany.filter.colorTranslater'
  ]);

  app.config(function($routeProvider) {
    $routeProvider.
      when('/', {
        templateUrl: 'orders/list/list.html',
        controller: 'ListCtrl'
      }).
      when('/detail/:id', {
        templateUrl: 'orders/detail/detail.html',
        controller: 'DetailCtrl',
        resolve: {
          detail: function(Orders) {
            return Orders.get({id: 1}).$promise.then(function(data) {
              return data;
            });
          }
        }
      })
      .otherwise({
        redirectTo: '/'
      });

  });

  app.constant('REST_BASE_URL', 'http://gdgangularteckovany.apiary-mock.com');

})(window.angular);