(function(angular) {
  'use strict';

  angular.module('administration.orders', [
    'administration.orders.list',
    'administration.orders.detail',
    'administration.orders.service'
  ]);

})(window.angular);