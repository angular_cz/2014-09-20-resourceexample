(function(angular) {
  'use strict';

  /* Services */

  var services = angular.module('administration.orders.service', ['ngResource']);

  services.factory('Orders', function($resource, REST_BASE_URL) {
    return $resource(REST_BASE_URL + '/orders/:id', {id: '@id'}, {
      query: {method: 'GET', isArray: true, params: {type: 'list'}},
      notify: {method: 'POST', params: {notified: 'true'}},
    }
    );
  });

})(window.angular);

